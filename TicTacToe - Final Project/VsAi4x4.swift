import UIKit

class VsAI4x4: UIViewController {
    var activePlayer = 1
    
    var gameState = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,0,0,0,0,0 ]
    
    let winningCombinations = [[0, 1, 2,3], [ 4, 5,6,7], [8,9,10,11], [12, 13, 14,15], [0, 4, 8,12], [ 1, 5,9,13], [2,6,10,14], [3, 7, 11,15],[0,5,10,15],[3,6,9,12]]

    var gameIsActive = true
    
    var gameTurns = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        // Do any additional setup after loading the view, typically from a nib.
        
        if arc4random_uniform(2)==1{
            aiPickRandom()
            gameTurns+=1
        }
        gamelabel.text = "Make your move, X."
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBOutlet weak var gamelabel: UILabel!
   
 
    
    @IBAction func gridButtonPress(_ sender: AnyObject) {
        
        gameTurns += 1
        
        
        
        
        if (gameState[sender.tag-1] == 0 && gameIsActive == true)
        {
            gameState[sender.tag-1] = 1
            
            //player picked a spot
            if activePlayer == 1{
                sender.setImage(#imageLiteral(resourceName: "x"), for: UIControlState())
                print("______player chose \(sender.tag-1)")
                checkWinner()
                
                
                
                activePlayer = 2
                gamelabel.text = "O's turn."
                
                gameIsActive = false
                for i in gameState
                {
                    if i == 0
                    {
                        gameIsActive = true
                        break
                    }
                }
                
                //no empty slots, no winners, draw.
                if gameIsActive == false
                {
                    gamelabel.text = "IT WAS A DRAW"
                    activePlayer=0
                }
                
                
                
                //Go for win
                if activePlayer==2{
                    goInWinSpot(playerNo: 2)
                }
                if activePlayer==2{
                    //Block player's potential win
                    goInWinSpot(playerNo: 1)
                }
                if gameTurns>=2 && activePlayer==2{
                    aiGoInForkSpot(playerNo: 2)
                }
                if gameTurns>=2 && activePlayer==2{
                    aiGoInForkSpot(playerNo: 1)
                }
                //pick the middle if it's empty
                if activePlayer==2{
                    if findAvailableMiddle() != -1{
                        print("picking available corner")
                        aiPickCell(cell: findAvailableMiddle())
                    }
                }
                if activePlayer==2{
                    if findAvailableCorner() != -1{
                        print("picking available corner")
                        aiPickCell(cell: findAvailableCorner())
                    }
                }
                if activePlayer==2{
                    if findAvailableSide() != -1{
                        print("picking available side")
                        aiPickCell(cell: findAvailableSide())
                    }
                }
                
                
                
            }
        }
        
        //if someone wins, gameIsActive = false
        checkWinner()
        
        
        gameIsActive = false
        
        //if there's still empty slots, game continues
        for i in gameState
        {
            if i == 0
            {
                gameIsActive = true
                break
            }
        }
        
        //no empty slots, no winners, draw.
        if gameIsActive == false
        {
            gamelabel.text = "IT WAS A DRAW"
            
        }
    }
    
    
    
    func aiPickCell(cell: Int){
        print("ai chooses \(cell)")
        gameState[cell] = 2
        let button = self.view.viewWithTag(cell+1) as! UIButton
        button.setImage(#imageLiteral(resourceName: "O"), for: UIControlState())
        activePlayer = 1
        gamelabel.text = "X's turn."
    }
    
    func goInWinSpot(playerNo: Int){
        print("checking if p\(playerNo) can win")
        for cellIndex in 0...gameState.count-1{
            //if cell is empty
            if gameState[cellIndex] == 0 && activePlayer == 2{
                
                //and player puts a cell here
                gameState[cellIndex] = playerNo
                
                //if this causes a win
                for combination in winningCombinations{
                    if gameState[combination[0]] != 0 && gameState[combination[0]] == gameState[combination[1]] && gameState[combination[1]] == gameState[combination[2]] && gameState[combination[2]] == gameState[combination[3]]{
                        print("player \(playerNo) is gonna win")
                        
                        //go there
                        if gameState[combination[0]]==playerNo{
                            aiPickCell(cell: cellIndex)
                        }
                    }
                }
                if activePlayer == 2{
                    gameState[cellIndex]=0
                }
            }
        }
    }
    
    func aiGoInForkSpot(playerNo: Int){
        print("checking for p\(playerNo) fork")
        //for every cell
        var bestCell = -1
        var potentialVictories = 0
        for cellIndex in 0...gameState.count-1{
            potentialVictories = 0
            //if cell is empty
            if gameState[cellIndex] == 0 && activePlayer == 2{
                
                //and player puts a cell here
                gameState[cellIndex] = playerNo
                
                //choose another cell, considering the first
                for cellIndex2 in 0...gameState.count-1{
                    
                    //if cell is empty
                    if gameState[cellIndex2] == 0 && activePlayer == 2{
                        
                        //and player puts a cell here
                        gameState[cellIndex2] = playerNo
                        
                        //if this causes a win
                        for combination in winningCombinations{
                            if gameState[combination[0]] != 0 && gameState[combination[0]] == gameState[combination[1]] && gameState[combination[1]] == gameState[combination[2]] && gameState[combination[2]] == gameState[combination[3]]{
                                
                                //wins from this situation
                                if gameState[combination[0]]==playerNo{
                                    potentialVictories+=1
                                    if (potentialVictories>=2){
                                        
                                        bestCell=cellIndex
                                        
                                    }
                                }
                            }
                            
                        }
                        gameState[cellIndex2]=0
                        
                    }
                }
                
                gameState[cellIndex]=0
            }
            
        }
        if potentialVictories>=2{
            print("player \(playerNo) could win via fork")
            aiPickCell(cell: bestCell)
        }
    }
    
    func findAvailableMiddle()->Int{
        for cellIndex in 0...gameState.count-1{
            if activePlayer==2{
                if cellIndex==5 && gameState[cellIndex]==0{
                    return 5
                }
                if cellIndex==6 && gameState[cellIndex]==0{
                    return 6
                }
                if cellIndex==9 && gameState[cellIndex]==0{
                    return 9
                }
                if cellIndex==10 && gameState[cellIndex]==0{
                    return 10
                }
            }
        }
        print("all mids full")
        return -1
    }
    
    
    func findAvailableCorner()->Int{
        for cellIndex in 0...gameState.count-1{
            if activePlayer==2{
                if cellIndex==0 && gameState[cellIndex]==0{
                    return 0
                }
                if cellIndex==3 && gameState[cellIndex]==0{
                    return 3
                }
                if cellIndex==12 && gameState[cellIndex]==0{
                    return 12
                }
                if cellIndex==15 && gameState[cellIndex]==0{
                    return 15
                }
            }
        }
        print("all corners full")
        return -1
    }
    
    func findAvailableSide()->Int{
        print("finding available side")
        for cellIndex in 0...gameState.count-1{
            if activePlayer==2{
                if cellIndex==1 && gameState[cellIndex]==0{
                    return 1
                }
                if cellIndex==2 && gameState[cellIndex]==0{
                    return 2
                }
                if cellIndex==4 && gameState[cellIndex]==0{
                    return 4
                }
                if cellIndex==8 && gameState[cellIndex]==0{
                    return 8
                }
                if cellIndex==7 && gameState[cellIndex]==0{
                    return 7
                }
                if cellIndex==11 && gameState[cellIndex]==0{
                    return 11
                }
                if cellIndex==13 && gameState[cellIndex]==0{
                    return 13
                }
                if cellIndex==14 && gameState[cellIndex]==0{
                    return 14
                }
            }
        }
        print("all sides full")
        return -1
    }
    
    func aiPickRandom(){
        let randomCell = Int(arc4random_uniform(16))
        aiPickCell(cell: randomCell)
    }
    
    func checkWinner(){
        for combination in winningCombinations
        {
            //4 in a row found.
            if gameState[combination[0]] != 0 && gameState[combination[0]] == gameState[combination[1]] && gameState[combination[1]] == gameState[combination[2]] && gameState[combination[2]] == gameState[combination[3]]
            {
                //game is done
                gameIsActive = false
                
                
                //it was either X or O.
                if gameState[combination[0]] == 1
                {
                    
                    gamelabel.text = "X HAS WON!"
                    activePlayer=0
                }
                else
                {
                    gamelabel.text = "O HAS WON!"
                    activePlayer=0
                }
            }
        }
        
    }
    
    
    
}
