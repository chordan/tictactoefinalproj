//
//  VsPlayer.swift
//  TicTacToe - Final Project
//
//  Created by Blake Eram on 2016-12-05.
//  Copyright © 2016 Harambe. All rights reserved.
//

import UIKit

class VsPlayer: UIViewController {
    
    
    var activePlayer = 1
    
    var gameState = [0, 0, 0, 0, 0, 0, 0, 0, 0]
    
    let winningCombinations = [[0, 1, 2], [3, 4, 5], [6, 7, 8], [0, 3, 6], [1, 4, 7], [2, 5, 8], [0, 4, 8], [2, 4, 6]]
    
    var gameIsActive = true
    
    
    @IBOutlet weak var gamelabel: UILabel!
  
    @IBAction func gridButtonPress(_ sender: AnyObject) {
        
        if (gameState[sender.tag-1] == 0 && gameIsActive == true)
        {
            gameState[sender.tag-1] = activePlayer
            
            //X chose a tile
            if activePlayer == 1{
                sender.setImage(#imageLiteral(resourceName: "x"), for: UIControlState())
                activePlayer = 2
                gamelabel.text = "O's turn."
            }
            
            //O chose a tile
            else if activePlayer == 2{
                sender.setImage(#imageLiteral(resourceName: "O"), for: UIControlState())
                activePlayer = 1
                gamelabel.text = "X's turn."
            }
        }
        
        
        
        
        //if someone wins, gameIsActive = false
        for combination in winningCombinations
        {
            if gameState[combination[0]] != 0 && gameState[combination[0]] == gameState[combination[1]] && gameState[combination[1]] == gameState[combination[2]]
            {
                gameIsActive = false
                
                if gameState[combination[0]] == 1
                {
                    gamelabel.text = "X HAS WON!"
                    activePlayer = 0
                   
                    
                }
                else
                {
                    gamelabel.text = "O HAS WON!"
                    activePlayer = 0
                }
                
            }
        }
        
        gameIsActive = false
        
        for i in gameState
        {
            if i == 0
            {
                gameIsActive = true
                break
            }
        }
        
        if gameIsActive == false
        {
            gamelabel.text = "IT WAS A DRAW"
       
        }
    }
    
    

   
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    
        gamelabel.text = "X goes first"

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    
    
}
